FROM csabasulyok/gradle:6.6.1-jdk8-alpine AS tmp
WORKDIR /usr/docker-web-app
COPY . ./
RUN gradle war

FROM tomcat:9.0.20-jre8-alpine
RUN rm -rf ./webapps/ROOT
COPY --from=tmp /usr/docker-web-app/siim1865-web/build/libs/siim1865-web-1.0-SNAPSHOT.war ./webapps/ROOT.war

