package edu.bbte.idde.siim1865.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigurationFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationFactory.class);
    private static final String DEFAULT_CONFIG_FILE = "/config.json";

    private static ObjectMapper objectMapper;

    private static Config mainConfiguration = new Config();

    public static final String CONFIG_FILE_ENV = "CONFIG_FILE";

    static {
        // JSON olvasó inicializálása
        objectMapper = new ObjectMapper();

        String configFile = System.getenv(CONFIG_FILE_ENV);

        if (configFile == null || configFile.isEmpty()) {
            configFile = DEFAULT_CONFIG_FILE;
        }
        // kérünk olvasási streamet a JSON állományhoz
        try (InputStream inputStream = ConfigurationFactory.class.getResourceAsStream(configFile)) {
            // JSON állomány beolvasása az általunk megadott osztály egy példányába
            mainConfiguration = objectMapper.readValue(inputStream, Config.class);
            LOG.info("Read following configuration: {} - {}", mainConfiguration, configFile);
        } catch (IOException e) {
            LOG.error("Error loading configuration", e);
        }
    }

    public static Config getMainConfiguration() {
        return mainConfiguration;
    }
}
