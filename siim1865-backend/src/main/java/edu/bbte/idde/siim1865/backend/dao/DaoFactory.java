package edu.bbte.idde.siim1865.backend.dao;

import edu.bbte.idde.siim1865.backend.config.ConfigurationFactory;
import edu.bbte.idde.siim1865.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.siim1865.backend.dao.memory.InMemoryDaoFactory;

public abstract class DaoFactory {

    private static DaoFactory INSTANCE;

    public abstract HikerDao getHikerDao();

    public abstract OrganizerDao getOrganizerDao();

    public static synchronized DaoFactory getFactory() {
        if (INSTANCE == null) {
            String daoType = ConfigurationFactory.getMainConfiguration().getDaoType();
            if ("mem".equals(daoType)) {
                INSTANCE = new InMemoryDaoFactory();
            } else if ("jdbc".equals(daoType)) {
                INSTANCE = new JdbcDaoFactory();
            } else {
                throw new DaoException("Could not read config file. ".concat(daoType));
            }

        }
        return INSTANCE;
    }
}
