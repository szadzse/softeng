package edu.bbte.idde.siim1865.backend.dao;

import edu.bbte.idde.siim1865.backend.model.Hiker;

import java.util.Collection;

public interface HikerDao {
    Collection<Hiker> findAll();

    Hiker findById(Integer id);

    void create(Hiker hiker);

    void deleteById(Integer id);

    void update(Integer id, Hiker hiker);
}

