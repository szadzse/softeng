package edu.bbte.idde.siim1865.backend.dao;

import edu.bbte.idde.siim1865.backend.model.Organizer;

import java.util.Collection;

public interface OrganizerDao {
    Collection<Organizer> findAll();

    Organizer findById(Integer id);

    void create(Organizer organizer);

    void deleteById(Integer id);

    void update(Integer id, Organizer organizer);
}
