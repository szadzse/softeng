package edu.bbte.idde.siim1865.backend.dao.jdbc;

import edu.bbte.idde.siim1865.backend.dao.DaoFactory;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.dao.OrganizerDao;

public class JdbcDaoFactory extends DaoFactory {

    private static HikerDao HIKERDAO;
    private static OrganizerDao ORGANIZERDAO;

    @Override
    public synchronized HikerDao getHikerDao() {
        if (HIKERDAO == null) {
            HIKERDAO = new JdbcHikerDao();
        }
        return HIKERDAO;
    }

    @Override
    public synchronized OrganizerDao getOrganizerDao() {
        if (ORGANIZERDAO == null) {
            ORGANIZERDAO = new JdbcOrganizerDao();
        }
        return ORGANIZERDAO;
    }
}
