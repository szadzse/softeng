package edu.bbte.idde.siim1865.backend.dao.jdbc;

import edu.bbte.idde.siim1865.backend.dao.DaoException;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.dao.jdbc.connectionpool.DataSource;
import edu.bbte.idde.siim1865.backend.model.Hiker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JdbcHikerDao implements HikerDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcHikerDao.class);

    @Override
    public Collection<Hiker> findAll() {

        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Hiker tabla listazasa");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT "
                    + "id, location, startDate, endDate, typeHiker, organizerId FROM hikers");
            List<Hiker> hikers = new ArrayList<>();
            while (resultSet.next()) {
                Hiker hiker = new Hiker();
                hiker.setId(resultSet.getInt(1));
                hiker.setLocation(resultSet.getString(2));
                hiker.setStartDate(resultSet.getDate(3));
                hiker.setEndDate(resultSet.getDate(4));
                hiker.setTypeHiker(resultSet.getString(5));
                hiker.setOrganizerId(resultSet.getInt(6));
                hikers.add(hiker);
            }
            return hikers;
        } catch (SQLException e) {
            LOGGER.error("Error when finding all hikers");
            throw new DaoException("Error when finding all hikers", e);
        }

    }

    @Override
    public Hiker findById(Integer id) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Kereses hikers tablaban id = {}", id);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, "
                    + "location, startDate, endDate, typeHiker, organizerId FROM hikers WHERE id =?");
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            Hiker hiker = new Hiker();
            if (resultSet.next()) {
                hiker.setId(resultSet.getInt(1));
                hiker.setLocation(resultSet.getString(2));
                hiker.setStartDate(resultSet.getDate(3));
                hiker.setEndDate(resultSet.getDate(4));
                hiker.setTypeHiker(resultSet.getString(5));
                hiker.setOrganizerId(resultSet.getInt(6));
            }
            return hiker;
        } catch (SQLException e) {
            LOGGER.error("Hiba id = {} szerinti keresesnel, hikers tablanal", id);
            throw new DaoException("Hiba id szerinti keresesnel, hikers tablanal", e);
        }
    }

    @Override
    public void create(Hiker hiker) {

        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Elem hozzaadasa az adatbazishoz");
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "
                    + "hikers (location, startDate, endDate, typeHiker) VALUES(?,?,?,?)");
            preparedStatement.setString(1, hiker.getLocation());
            preparedStatement.setDate(2, hiker.getStartDate());
            preparedStatement.setDate(3, hiker.getEndDate());
            preparedStatement.setString(4, hiker.getTypeHiker());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Error when creating");
            throw new DaoException("Error when creating", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Tolrles {} hiba", id);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE "
                    + "hikers WHERE  id=?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Error when delete");
            throw new DaoException("Error when delete", e);
        }
    }

    @Override
    public void update(Integer id, Hiker hiker) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Update {} hiba", id);
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE"
                    + " hikers set location = ?, startDate=?, endDate=?, typeHiker=? WHERE  id=?");
            preparedStatement.setString(1, hiker.getLocation());
            preparedStatement.setDate(2, Date.valueOf("2020-10-11"));
            preparedStatement.setDate(3, Date.valueOf("2020-11-11"));
            preparedStatement.setString(4, hiker.getTypeHiker());
            preparedStatement.setString(5, String.valueOf(id));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Error when updating");
            throw new DaoException("Error when updating", e);
        }
    }
}
