package edu.bbte.idde.siim1865.backend.dao.jdbc;

import edu.bbte.idde.siim1865.backend.dao.DaoException;
import edu.bbte.idde.siim1865.backend.dao.OrganizerDao;
import edu.bbte.idde.siim1865.backend.dao.jdbc.connectionpool.DataSource;
import edu.bbte.idde.siim1865.backend.model.Organizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JdbcOrganizerDao implements OrganizerDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcOrganizerDao.class);

    public void resultSetMap(ResultSet resultSet, Organizer organizer) throws SQLException {
        organizer.setId(resultSet.getInt(1));
        organizer.setName(resultSet.getString(2));
        organizer.setPhone(resultSet.getString(3));
    }

    public void preparedStatementSetMap(PreparedStatement preparedStatement, Organizer organizer) throws SQLException {
        preparedStatement.setString(1, organizer.getName());
        preparedStatement.setString(2, organizer.getPhone());
    }

    @Override
    public Collection<Organizer> findAll() {
        try (Connection connection = DataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT "
                    + "id, name, phone FROM organizers");
            List<Organizer> organizers = new ArrayList<>();
            while (resultSet.next()) {
                Organizer organizer = new Organizer();
                resultSetMap(resultSet, organizer);
                organizers.add(organizer);
            }
            return organizers;
        } catch (SQLException e) {
            LOGGER.error("Error when finding all organizes");
            throw new DaoException("Error when finding all organizers", e);
        }
    }

    @Override
    public Organizer findById(Integer id) {

        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Kereses organizers tablaban id = {}", id);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, "
                    + "name, phone FROM organizers WHERE id =?");
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            Organizer organizer = new Organizer();
            if (resultSet.next()) {
                resultSetMap(resultSet, organizer);
            }
            return organizer;
        } catch (SQLException e) {
            LOGGER.error("Hiba id = {} szerinti keresesnel, organizers tablanal ", id);
            throw new DaoException("Hiba id szerinti keresesnel, organizers tablanal", e);
        }
    }

    @Override
    public void create(Organizer organizer) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Elem hozzaadasa az adatbazishoz");
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "
                    + "organizers (name, phone) VALUES(?,?)");
            preparedStatementSetMap(preparedStatement, organizer);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Error when creating");
            throw new DaoException("Error when creating", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Tolrles {} hiba", id);
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE "
                    + "organizers WHERE  id=?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Error when delete");
            throw new DaoException("Error when delete", e);
        }
    }

    @Override
    public void update(Integer id, Organizer organizer) {
        try (Connection connection = DataSource.getConnection()) {
            LOGGER.info("Update {} hiba", id);
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE"
                    + " organizers set name = ?, phone=?  WHERE  id=?");
            preparedStatementSetMap(preparedStatement, organizer);
            preparedStatement.setString(3, String.valueOf(id));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Error when updating");
            throw new DaoException("Error when updating", e);
        }

    }
}
