package edu.bbte.idde.siim1865.backend.dao.jdbc.connectionpool;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.siim1865.backend.config.Config;
import edu.bbte.idde.siim1865.backend.config.ConfigurationFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static HikariDataSource hikariDataSource;

    public static synchronized Connection getConnection() throws SQLException {
        if (hikariDataSource == null) {
            hikariDataSource = createDataSource();
        }
        return hikariDataSource.getConnection();
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        Config config = ConfigurationFactory.getMainConfiguration();
        hikariConfig.setDriverClassName(config.getDriverClassName());
        hikariConfig.setJdbcUrl(config.getJdbcUrl());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        hikariConfig.setMaximumPoolSize(10);
        return new HikariDataSource(hikariConfig);
    }
}
