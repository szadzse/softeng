package edu.bbte.idde.siim1865.backend.dao.memory;

import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.model.Hiker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class HikerInMemoryDao implements HikerDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(HikerInMemoryDao.class);
    private static final Map<Integer, Hiker> HIKER_MAP = new ConcurrentHashMap<>();
    private static final AtomicInteger ID_COUNTER = new AtomicInteger();

    public HikerInMemoryDao() {
        create(new Hiker("KisKend", new java.sql.Date(Calendar.getInstance().getTime().getTime()),
                new java.sql.Date(Calendar.getInstance().getTime().getTime() + 10), "bicikli tura", 1));
        create(new Hiker("NagyKend", new java.sql.Date(Calendar.getInstance().getTime().getTime()),
                new java.sql.Date(2020, 12, 01), "bicikli tura", 1));
    }

    @Override
    public Collection<Hiker> findAll() {
        LOGGER.info("Visszateritve az osszes");
        return HIKER_MAP.values();
    }

    @Override
    public Hiker findById(Integer id) {
        LOGGER.info("Visszateritve ID alapjan {}", id);
        return HIKER_MAP.get(id);
    }

    @Override
    public void create(Hiker hiker) {
        Integer newID = ID_COUNTER.getAndIncrement();
        hiker.setId(newID);
        HIKER_MAP.put(newID, hiker);
        LOGGER.info("Hozzaadva {}", hiker);
    }

    @Override
    public void deleteById(Integer id) {
        HIKER_MAP.remove(id);
        LOGGER.info("Torolve {}", id);
    }

    @Override
    public void update(Integer id, Hiker hiker) {
        hiker.setId(id);
        HIKER_MAP.replace(id, hiker);
        LOGGER.info("Frisitve {}", id);
    }

}
