package edu.bbte.idde.siim1865.backend.dao.memory;

import edu.bbte.idde.siim1865.backend.dao.DaoFactory;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.dao.OrganizerDao;

public class InMemoryDaoFactory extends DaoFactory {

    private HikerDao hikerInMemoryDao;
    private OrganizerDao organizerInMemoryDao;

    @Override
    public HikerDao getHikerDao() {
        if (hikerInMemoryDao == null) {
            hikerInMemoryDao = new HikerInMemoryDao();
        }
        return hikerInMemoryDao;
    }

    @Override
    public OrganizerDao getOrganizerDao() {
        if (organizerInMemoryDao == null) {
            organizerInMemoryDao = new OrganizerInMemoryDao();
        }
        return organizerInMemoryDao;
    }
}
