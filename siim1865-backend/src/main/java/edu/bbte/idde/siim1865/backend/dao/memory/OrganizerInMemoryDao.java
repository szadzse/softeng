package edu.bbte.idde.siim1865.backend.dao.memory;

import edu.bbte.idde.siim1865.backend.dao.OrganizerDao;
import edu.bbte.idde.siim1865.backend.model.Organizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class OrganizerInMemoryDao implements OrganizerDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizerInMemoryDao.class);
    private static final Map<Integer, Organizer> ORGANIZER_MAP = new ConcurrentHashMap<>();
    private static final AtomicInteger IDCOUNTER = new AtomicInteger();

    @Override
    public Collection<Organizer> findAll() {
        LOGGER.info("Visszateritve az osszes");
        return ORGANIZER_MAP.values();
    }

    @Override
    public Organizer findById(Integer id) {
        LOGGER.info("Visszateritve ID alapjan {}", id);
        return ORGANIZER_MAP.get(id);
    }

    @Override
    public void create(Organizer organizer) {
        Integer newID = IDCOUNTER.getAndIncrement();
        organizer.setId(newID);
        ORGANIZER_MAP.put(newID, organizer);
        LOGGER.info("Hozzaadva {}", organizer);
    }

    @Override
    public void deleteById(Integer id) {
        ORGANIZER_MAP.remove(id);
        LOGGER.info("Torolve {}", id);
    }

    @Override
    public void update(Integer id, Organizer organizer) {
        organizer.setId(id);
        ORGANIZER_MAP.replace(id, organizer);
        LOGGER.info("Frisitve {}", id);
    }

}
