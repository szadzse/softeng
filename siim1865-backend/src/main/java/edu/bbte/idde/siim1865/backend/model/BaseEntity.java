
package edu.bbte.idde.siim1865.backend.model;

import java.io.Serializable;

public abstract class BaseEntity implements Serializable {
    protected Integer id;

    public BaseEntity() {
    }

    public BaseEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}