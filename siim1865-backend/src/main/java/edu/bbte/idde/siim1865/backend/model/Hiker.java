package edu.bbte.idde.siim1865.backend.model;

import java.sql.Date;

public class Hiker extends BaseEntity {
    private String location;
    private Date startDate;
    private Date endDate;
    private String typeHiker;
    private Integer organizerId;

    public Hiker() {
        super();
    }

    public Hiker(String location, Date startDate, Date endDate, String typeHiker, Integer organizerId) {
        super();
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.typeHiker = typeHiker;
        this.organizerId = organizerId;
    }

    public Integer getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(Integer organizerId) {
        this.organizerId = organizerId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTypeHiker() {
        return typeHiker;
    }

    public void setTypeHiker(String typeHiker) {
        this.typeHiker = typeHiker;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Hiker{");
        sb.append("location='").append(location).append('\'');
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", typeHiker='").append(typeHiker).append('\'');
        sb.append(", organizerId=").append(organizerId);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
