package edu.bbte.idde.siim1865.frontend;

import edu.bbte.idde.siim1865.backend.dao.DaoFactory;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.model.Hiker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    static JFrame frame;
    static JButton button = new JButton();
    static JPanel panel = new JPanel();
    static JScrollPane scrollPane = new JScrollPane(new JTable(0, 0));
    static HikerDao hikerDao = DaoFactory.getFactory().getHikerDao();

    private static JTable update() {

        int i = 0;

        String[][] data = new String[hikerDao.findAll().size()][5];
        String[] temp = {"id", "location", "startDate", "endDate", "typeHiker", "organizerId"};

        LOGGER.info("Tablazatositas");

        for (Hiker hiker : hikerDao.findAll()) {
            LOGGER.info(hiker.toString());
            data[i][0] = String.valueOf(hiker.getId());
            data[i][1] = hiker.getLocation();
            data[i][2] = String.valueOf(hiker.getStartDate());
            data[i][3] = String.valueOf(hiker.getEndDate());
            data[i][4] = hiker.getTypeHiker();
            data[i][5] = String.valueOf(hiker.getOrganizerId());
            i++;
        }

        //JTable table = new JTable(data,temp);
        return new JTable(data, temp);
    }

    public static void main(String[] args) {

        frame = new JFrame();
        frame.setTitle("Turak");
        //rd.logging();
        hikerDao.create(new Hiker("Kend", new java.sql.Date(2020, 11, 02),
                new java.sql.Date(2020, 11, 20), "bicikli tura", 1));
        hikerDao.create(new Hiker("Kend", new java.sql.Date(2020, 11, 02),
                new java.sql.Date(2020, 11, 20), "bicikli tura", 1));
        hikerDao.update(1, new Hiker("Lorincfala", new java.sql.Date(2020, 11, 02),
                new java.sql.Date(2021, 8, 12), "gyalogtura", 1));

        LOGGER.info("Hozzaadtunk frontenden");

        button.addActionListener(e -> {
            scrollPane.setViewportView(update());
            frame.repaint();
        });

        button.setVisible(true);
        button.setText("Update");

        panel.add(button);
        panel.add(scrollPane);

        frame.add(panel);
        frame.setSize(700, 350);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    }
}
