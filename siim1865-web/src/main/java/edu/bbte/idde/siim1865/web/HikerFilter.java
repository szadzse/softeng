package edu.bbte.idde.siim1865.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/hiker.hbs")
public class HikerFilter extends HttpFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(HikerFilter.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res,
                            FilterChain chain) throws IOException, ServletException {
        LOGGER.info(" Method: {}, URL: {}, StatusCode: {}", req.getMethod(), req.getRequestURL(), res.getStatus());
        if (req.getSession().getAttribute("username") == null) {
            res.sendRedirect(req.getContextPath() + "/login.html");
        } else {
            chain.doFilter(req, res);
        }
    }
}
