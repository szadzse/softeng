package edu.bbte.idde.siim1865.web;

import edu.bbte.idde.siim1865.backend.dao.DaoFactory;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.dao.OrganizerDao;
import edu.bbte.idde.siim1865.backend.model.Hiker;
import edu.bbte.idde.siim1865.backend.model.Organizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Collection;

@WebServlet("/hikers")
public class HikerOrganizerServlet extends HttpServlet {
    private static final HikerDao HIKER_DAO = DaoFactory.getFactory().getHikerDao();
    private static final OrganizerDao ORGANIZER_DAO = DaoFactory.getFactory().getOrganizerDao();
    private static final Logger LOGGER = LoggerFactory.getLogger(HikerOrganizerServlet.class);
    HikerDao hikerDao;

    @Override
    public void init() throws ServletException {
        hikerDao = DaoFactory.getFactory().getHikerDao();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");

        PrintWriter writer = resp.getWriter();

        if (id == null) {
            Collection<Hiker> hikers = HIKER_DAO.findAll();
            for (Hiker hiker : hikers) {
                writer.println(hiker.toString());
            }
            Collection<Organizer> organizers = ORGANIZER_DAO.findAll();
            for (Organizer organizer: organizers) {
                writer.println(organizer.toString());
            }
            LOGGER.info("/hikers GET");
        } else {
            try {
                Hiker hiker = HIKER_DAO.findById(Integer.parseInt(id));
                if (hiker == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    writer.println(hiker);
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String location = req.getParameter("loc");
            Date startdate = new Date(Long.parseLong(req.getParameter("sd")));
            Date enddate = new Date(Long.parseLong(req.getParameter("ed")));
            String typeHiker = req.getParameter("ht");
            Integer organizerId = Integer.parseInt(req.getParameter("vid"));
            LOGGER.info("{}  {}  {}  {} {}", location, startdate, enddate, typeHiker, organizerId);
            if (location == null || startdate == null || enddate == null || typeHiker == null || organizerId == null) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                LOGGER.info("/hikers POST hianyos keres");
                LOGGER.info("{}  {}  {}  {} {}", location, startdate, enddate, typeHiker, organizerId);
            } else {
                hikerDao.create(new Hiker(location, startdate, enddate, typeHiker, organizerId));
                LOGGER.info("/hikers POST");
                resp.sendRedirect(req.getContextPath() + "/hiker.hbs");
            }
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            LOGGER.info("Exception {}", e);
        }

    }
}
