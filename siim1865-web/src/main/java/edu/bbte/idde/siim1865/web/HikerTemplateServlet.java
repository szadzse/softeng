package edu.bbte.idde.siim1865.web;

import com.github.jknack.handlebars.Template;
import edu.bbte.idde.siim1865.backend.dao.DaoFactory;
import edu.bbte.idde.siim1865.backend.dao.HikerDao;
import edu.bbte.idde.siim1865.backend.model.Hiker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/hiker.hbs")
public class HikerTemplateServlet extends HttpServlet {
    private static final HikerDao HIKER_DAO = DaoFactory.getFactory().getHikerDao();
    private static final Logger LOGGER = LoggerFactory.getLogger(HikerOrganizerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Template template = HandlebarsTemplateFactory.getTemplate("hiker");
        Collection<Hiker> hikers = HIKER_DAO.findAll();
        template.apply(hikers, resp.getWriter());
        LOGGER.info("/hiker.hbs GET");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        LOGGER.info("session ok torolve");
        resp.sendRedirect(req.getContextPath() + "/hiker.hbs");
    }
}