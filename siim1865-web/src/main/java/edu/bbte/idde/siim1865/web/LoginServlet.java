package edu.bbte.idde.siim1865.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    private static String username = "admin";
    private static String password = "admin";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (username.equals(req.getParameter("usern")) && password.equals(req.getParameter("ps"))) {
            req.getSession().setAttribute("username", username);
            resp.sendRedirect(req.getContextPath() + "/hiker.hbs");
        } else {
            LOGGER.info("hibas jelszo");
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
