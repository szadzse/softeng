-- docker-compose exec -T hiker-db /usr/local/bin/psql hikerdb hiker-user hiker-pass <sql_scripts/create.sql
DROP TABLE IF EXISTS hikers;
CREATE TABLE hikers
(
    id          bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    location    varchar(30),
    startDate  date,
    endDate    date,
    typeHiker  varchar(30),
    organizerId int default null,
    foreign key (organizerId) references organizers (id)
);

CREATE TABLE organizers
(
    id      bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name    varchar(30),
    phone  varchar(30)
);

INSERT INTO hikers(location, startDate, endDate, typeHiker, organizerId)
VALUES ('Szatmarnemeti', '2020-04-05', '2020-04-05', 'kenu', 1);

INSERT INTO hikers(location, startDate, endDate, typeHiker, organizerId)
VALUES ('Kolozsvar', '2020-08-05', '2020-08-05', 'bicikli', 2);

INSERT INTO organizers(name, phone)
VALUES ('Attila','0746861056');

INSERT INTO organizers(name, phone)
VALUES ('Istvan','0773861056');